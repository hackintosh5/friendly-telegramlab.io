---
title: Automated Installation
layout: default
parent: Installation
nav_order: 1
---

# {{ page.title }}

If you want to host on a **Linux computer**, a **Mac** or an **Android** phone, paste this command into Termux (an app on the Play Store) or a terminal:
```
(. <($(which curl>/dev/null&&echo curl -Ls||echo wget -qO-) https://kutt.it/ftgi))
```

If you want to host on **Windows** (7 and higher) paste this command into [Windows Powershell](http://www.powertheshell.com/topic/learnpowershell/firststeps/console):
```
iex (New-Object Net.WebClient).DownloadString("https://kutt.it/ftgp")
```
Or for Heroku on **Windows**:
```
iex (New-Object Net.WebClient).DownloadString("https://kutt.it/ftgh")
```

If you want to host on **PythonAnywhere** (and other GNU-like platforms that don't have `/dev/fd/*` or bash named pipe support)
```
$(which curl>/dev/null&&echo curl -LsO||echo wget -q) https://kutt.it/ftgi&&(. JeOXn --no-web);rm ftgi
```
